"Inspirations https://github.com/xero/dotfiles

set langmap='q,\\,w,.e,pr,yt,fy,gu,ci,ro,lp,/[,=],aa,os,ed,uf,ig,dh,hj,tk,nl,s\\;,-',\\;z,qx,jc,kv,xb,bn,mm,w\\,,v.,z/,[-,]=,\"Q,<W,>E,PR,YT,FY,GU,CI,RO,LP,?{,+},AA,OS,ED,UF,IG,DH,HJ,TK,NL,S:,_\",:Z,QX,JC,KV,XB,BN,MM,W<,V>,Z?

let mapleader=","

set nocompatible

call plug#begin('~/.config/nvim/plugged')

Plug 'dense-analysis/ale'
Plug 'ryanoasis/vim-devicons'
Plug 'majutsushi/tagbar'
Plug 'airblade/vim-gitgutter'
Plug 'mhinz/vim-signify'

Plug 'scrooloose/syntastic'
Plug 'kien/rainbow_parentheses.vim'
Plug 'neovimhaskell/haskell-vim'
Plug 'sheerun/vim-polyglot'

Plug 'neoclide/coc.nvim', {'do': 'yarn install --frozen-lockfile'}
Plug 'tpope/vim-dispatch'

Plug 'ctrlpvim/ctrlp.vim' " fuzzy find fils
Plug 'dyng/ctrlsf.vim'
Plug 'kovetskiy/sxhkd-vim'

Plug 'jreybert/vimagit'
Plug 'ap/vim-css-color'

Plug 'nikolvs/vim-sunbather'
Plug 'arcticicestudio/nord-vim'

call plug#end()

set go=a

set clipboard=unnamed,unnamedplus

let base16colorspace=256


let $NVIM_TUI_ENABLE_TRUE_COLOR = 1
set t_Co=256
set t_ut=
set termguicolors

nnoremap c "_c
filetype plugin indent on
syntax on
set encoding=utf-8
set number

set hidden
set updatetime=300
set shortmess+=c
set signcolumn=no

set wildmode=longest,list,full
" Disables automatic commenting on newline
    autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Runs a script that clean out tex build files whenever I close vim
    autocmd VimLeave *.tex !texclear %

" Save file as sudo on files that require root permission
    cnoremap w!! execute 'silent! write !sudo tee % >/dev/null' <bar> edit!

" Turn off swap files
    set noswapfile
    set nobackup
    set nowb

" Indentation
    set autoindent
    set smartindent
    set shiftwidth=4
    set tabstop=4
    set smarttab
    set expandtab

" Search
    set hlsearch
    set incsearch
    set ignorecase
    set smartcase

" Fast scrolling
    noremap H 5h
    noremap J 5j
    noremap K 5k
    noremap L 5l


" Tab navigation
    noremap <C-Left> :tabprevious<CR>
    noremap <C-Right> :tabnext<CR>

" Run xrdb whenever Xdefaults or Xresources are updated.
    autocmd BufWritePre *Xresources,*Xdefaults !xrdb %

" Run source% whenever init.vim is updated
    autocmd BufWritePre *init.vim source%

" Ale (has settings for lightline delphinus)
    let g:ale_echo_msg_error_str = nr2char(0xf421) . ' '
    let g:ale_echo_msg_warning_str = nr2char(0xf420) . ' '
    let g:ale_echo_msg_info_str = nr2char(0xf05a) . ' '
    let g:ale_echo_msg_format = '%severity%  %linter% - %s'
    let g:ale_sign_column_always = 1
    let g:ale_sign_error = g:ale_echo_msg_error_str
    let g:ale_sign_warning = g:ale_echo_msg_warning_str
    let g:ale_statusline_format = [
          \ g:ale_echo_msg_error_str . ' %d',
          \ g:ale_echo_msg_warning_str . ' %d',
          \ nr2char(0xf4a1) . '  ']


" Abbreviations
    cnoreabbrev W! w!
    cnoreabbrev Q! q!
    cnoreabbrev Qall! qall!
    cnoreabbrev Wq wq
    cnoreabbrev Wa wa
    cnoreabbrev wQ wq
    cnoreabbrev WQ wq
    cnoreabbrev W w
    cnoreabbrev Q q
    cnoreabbrev Qall qall

" COC
    " Highllight the symbol and its references when holding the cursor.
  " autocmd CursorHold * silent call CocActionAsync('highlight')


" Haskell
    let g:haskell_classic_highlighting = 1
    let g:haskell_indent_if = 3
    let g:haskell_indent_case = 2
    let g:haskell_indent_let = 4
    let g:haskell_indent_where = 6
    let g:haskell_indent_before_where = 2
    let g:haskell_indent_after_bare_where = 2
    let g:haskell_indent_do = 3
    let g:haskell_indent_in = 1
    let g:haskell_indent_guard = 2
    let g:haskell_indent_case_alternative = 1
    let g:cabal_ident_section = 2

" Deoplete
"    let g:deoplete#enable_at_startup = 1

set cursorline

cmap <leader>r :source ~/.config/nvim/init.vim<CR>

" Switching windows
noremap <C-h> <C-w>j
noremap <C-t> <C-w>k
noremap <C-n> <C-w>l
noremap <C-d> <C-w>h

nnoremap <C-s> :split<CR>
map <C-b> :Buffers<CR>

noremap <A-d> :vertical resize -5<CR>
noremap <A-n> :vertical resize +5<CR>
noremap <A-h> :resize -5<CR>
noremap <A-t> :resize +5<CR>

" Vmap for maintain visual mode after shifting > and <
vmap < <gv
vmap > >gv

" Search mappings: These will make it so that going to the next one in a 
" Search will center on the line it's found in.
nnoremap n nzzzv
nnoremap N Nzzzv

autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | exe 'F' | endif

autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'F' argv()[0] | endif


" Turns off highlighting on the bits of code that are changed, so the line that is changed is highlighted but the actual text that has changed stands out on the line and is readable.
if &diff
    highlight! link DiffText MatchParen
endif

" Hide statusline
set laststatus=0

" Change highlight of cursor line when entering/leaving Insert Mode
set cursorline
highlight CursorLine cterm=NONE ctermfg=NONE ctermbg=233 guifg=NONE guibg=#121212
autocmd InsertEnter * highlight CursorLine cterm=NONE ctermfg=NONE ctermbg=234 guifg=NONE guibg=#1c1c1c
autocmd InsertLeave * highlight CursorLine cterm=NONE ctermfg=NONE ctermbg=233 guifg=NONE guibg=#121212


colorscheme nord



