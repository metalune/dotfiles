import os
import sys
from dotfiles_config import config_files, directories

for dir in directories:
    os.system("mkdir -p \"{}\"".format(dir))


for file in config_files:
    os.system("cp -a \"{}\" \"{}\"".format(file, config_files[file]))

