config_files = {
    # NEOVIM
    "$HOME/.config/nvim/init.vim": "config/nvim/init.vim",
    # QUTEBROWSER
    "$HOME/.config/qutebrowser/config.py": "config/qutebrowser/config.py",
    # DUNST
    "$HOME/.config/dunst/dunstrc": "config/dunst/dunstrc",
    # COC
    "$HOME/.config/coc/": "config",
    # Newsboat
    "$HOME/.config/newsboat/config": "config/newsboat/config",
    # Fontconfig
    "$HOME/.config/fontconfig/": "config",
    # XORG
    "$HOME/.Xmodmap": "Xmodmap",
    "$HOME/.xinitrc": "xinitrc",
    "$HOME/.xprofile": "xprofile",
    "$HOME/.config/Xresources": "config/Xresources",
    # BASH
    "$HOME/.bashrc": "bashrc",
    "$HOME/.bash_profile": "bash_profile",
    # SHELL
    "$HOME/.config/shell/": "config",
    # ZSH
    "$HOME/.zprofile": "zprofile",
    "$HOME/.zshrc": "zshrc",
    "$HOME/.config/zsh/": "config",
    # NCMPCPP
    "$HOME/.config/ncmpcpp/config":             "config/ncmpcpp/config",
    "$HOME/.config/ncmpcpp/bindings":           "config/ncmpcpp/bindings",
    # RANGER
    "$HOME/.config/ranger/rc.conf":             "config/ranger/rc.conf",
    "$HOME/.config/ranger/commands.py":         "config/ranger/commands.py",
    # PICOM
    "$HOME/.config/picom/picom.conf":           "config/picom/picom.conf",
    # ZATHURA
    "$HOME/.config/zathura/zathurarc":          "config/zathura/zathurarc",
    # STARSHIP
    "$HOME/.config/starship.toml": "config/starship.toml",


    # LOCAL BIN
    "$HOME/.local/bin/rssadd": "local/bin/rssadd",
    "$HOME/.local/bin/remaps": "local/bin/remaps",
    "$HOME/.local/bin/texclear": "local/bin/texclear",
    "$HOME/.local/bin/displayselect": "local/bin/displayselect",
    "$HOME/.local/bin/dmenu-kill-process": "local/bin/dmenu-kill-process",
    "$HOME/.local/bin/dmenumount": "local/bin/dmenumount",
    "$HOME/.local/bin/dmenuumount": "local/bin/dmenuumount",
    "$HOME/.local/bin/dmenupass": "local/bin/dmenupass",
    "$HOME/.local/bin/statusbar": "local/bin",
    "$HOME/.local/bin/bg-setter": "local/bin",

    # POEZIO
    "$HOME/.config/poezio": "config",
}

directories = (
    "local/bin",
    "local/share",
    "config/newsboat",
    "config/dunst",
    "config/shell",
    "config/ncmpcpp",
    "config/nvim",
    "config/ranger",
    "config/picom",
    "config/poezio",
    "config/qutebrowser",
    "config/zathura",
    "config/zsh",
)
