#!/bin/zsh

# Adds `~/.local/bin` to $PATH
export PATH="$PATH:$(du "$HOME/.local/bin" | cut -f2 | paste -sd ':')"

export XKB_DEFAULT_LAYOUT=us
export XKB_DEFAULT_VARIANT=dvorak

# Default programs:
export EDITOR="nvim"
export TERMINAL="st"
export BROWSER="firefox"
export READER="zathura"


# ~/ Clean-up:
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"

# FFF config
export FFF_KEY_PARENT1=d
export FFF_KEY_SCROLL_DOWN1=h
export FFF_KEY_SCROLL_UP1=t
export FFF_KEY_CHILD1=n
export FFF_KEY_SEARCH=z
export FFF_KEY_YANK=f
export FFF_KEY_YANK_ALL=F
export FFF_KEY_TRASH=e
export FFF_KEY_TRASH_ALL=E
export FFF_KEY_LINK=o
export FFF_KEY_LINK_ALL=O
export FFF_KEY_BULK_RENAME=x
export FFF_KEY_BULK_RENAME_ALL=X
export FFF_KEY_PASTE=l
export FFF_KEY_CLEAR=j
export FFF_KEY_RENAME=p
export FFF_KEY_MKDIR=b
export FFF_KEY_MKFILE=u
export FFF_KEY_ATTRIBUTES=b
export FFF_KEY_EXECUTABLE=B
export FFF_KEY_IMAGE=c
export FFF_KEY_GO_DIR=S
export FFF_KEY_GO_TRASH=y
export FFF_KEY_PREVIOUS="["
export FFF_KEY_REFRESH="."
export FFF_KEY_TO_TOP=i
export FFF_KEY_TO_BOTTOM=I
export FFF_KEY_HIDDEN=v
export FFF_FAV1=/mnt/TittyDrive

export PATH="$HOME/.cargo/bin:$PATH"
